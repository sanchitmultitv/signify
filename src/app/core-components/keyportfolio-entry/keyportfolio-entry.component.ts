import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-keyportfolio-entry',
  templateUrl: './keyportfolio-entry.component.html',
  styleUrls: ['./keyportfolio-entry.component.scss']
})
export class KeyportfolioEntryComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }
  enterKeyPortfolio(){
this.router.navigate(['/keyportfolio']);
  }
}
