import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
import * as _ from 'underscore';
import * as Clappr from 'clappr';

@Component({
  selector: 'app-auditoriumfour',
  templateUrl: './auditoriumfour.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})

export class AuditoriumfourComponent implements OnInit , OnDestroy {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_17';
  serdia_room = localStorage.getItem('serdia_room');
  player:any
  newWidth;
  newHeight;
  videoEnd = false;
  // videoPlayerFour = 'assets/lobby-video.mp4';
  videoPlayerFour= 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  interval;

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
     this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
     this.playVideo();

  } 
  playVideo() {
    var playerElement = document.getElementById("player_sec");
    this.player = new Clappr.Player({
      parentId: 'player_sec',
      source: this.videoPlayerFour,
      // source: "http://d3tn0h9cityxqm.cloudfront.net/streamline/chunks/701_5ef2e2fc9223a/701_5ef2e2fc9223a_master.m3u8",
      poster: 'assets/sat.jpg',
      height: '90%',
      maxBufferLength: 30,
      width: '100%',
      autoPlay: true,
      loop: true,
      hideMediaControl: false,
      hideVolumeBar: false,
      hideSeekBar: false,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
    // if (window.innerWidth <= 572) {
    //   this.player.play();
    //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
    // } else {
    //   this.player.play();
    //   var aspectRatio = 9 / 16;
    //   // tslint:disable-next-line: prefer-const
    //   this.newWidth = document.getElementById('player-wrapper').parentElement
    //     .offsetWidth;
    //   this.newHeight = 2 * Math.round((this.newWidth * aspectRatio) / 2);
    //   if (this.newWidth < 1000) {
    //   this.player.resize({ width: this.newWidth, height: this.newHeight });
    //   $('.container')
    //     .find('.player-poster')
    //     .css('background-size', 'contain');
    // }
    // }
  }
  getHeartbeat(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '140');
    formData.append('audi', '4');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatTwo').modal('show')
    this.loadData();
  }

  loadData(): void {
    this.chatGroupTwo();
    this.chatService.getconnect('toujeo-52');
    // this.chatService.getMessages().subscribe((data => {
    //   if (data == 'group_chat') {
    //     this.chatGroupTwo();
    //   }
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages(this.serdia_room)
      .subscribe((msgs: any) => {
        if (msgs.roomId === 2){
          this.messageList.push(msgs);
        }        
        console.log('demo', this.messageList);
      });
  }
  chatGroupTwo() {
    this._fd.groupchatingtwo().subscribe(res => {
      console.log('res', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatTwo').modal('hide');
  }
  postMessageTwo(value) {
    // console.log(value);
    let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(data.name);
    this.chatService.sendMessage(value, data.name, this.serdia_room);
    // console.log(this.roomName);
    this.textMessage.reset();
    // this.chatGroupTwo();
    //this.newMessage.push(this.msgs);
  }
  ngOnDestroy(){
    clearInterval(this.interval);
  }
}

