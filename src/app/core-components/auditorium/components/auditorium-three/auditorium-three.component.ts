import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
declare var $: any;
import * as _ from 'underscore';
import * as Clappr from 'clappr';
@Component({
  selector: 'app-auditorium-three',
  templateUrl: './auditorium-three.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})

export class AuditoriumThreeComponent implements OnInit, OnDestroy {
  videoEnd = false;
  // videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  interval;
  player:any
  newWidth;
  newHeight;
   videoPlayerThree = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session3.smil/playlist.m3u8';
  // videoPlayerThree = 'assets/mov_bbb.mp4';

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
   this.playVideo();

    // console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-140');
    this.chatService.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'groupchat') {
        this.chatGroup();
      }

    }));
  }
  playVideo() {
    var playerElement = document.getElementById("player_sec");
    this.player = new Clappr.Player({
      parentId: 'player_sec',
      source: this.videoPlayerThree,
      // source: "http://d3tn0h9cityxqm.cloudfront.net/streamline/chunks/701_5ef2e2fc9223a/701_5ef2e2fc9223a_master.m3u8",
      poster: 'assets/sat.jpg',
      height: '90%',
      maxBufferLength: 30,
      width: '100%',
      autoPlay: true,
      loop: true,
      hideMediaControl: false,
      hideVolumeBar: false,
      hideSeekBar: false,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
    // if (window.innerWidth <= 572) {
    //   this.player.play();
    //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
    // } else {
    //   this.player.play();
    //   var aspectRatio = 9 / 16;
    //   // tslint:disable-next-line: prefer-const
    //   this.newWidth = document.getElementById('player-wrapper').parentElement
    //     .offsetWidth;
    //   this.newHeight = 2 * Math.round((this.newWidth * aspectRatio) / 2);
    //   if (this.newWidth < 1000) {
    //   this.player.resize({ width: this.newWidth, height: this.newHeight });
    //   $('.container')
    //     .find('.player-poster')
    //     .css('background-size', 'contain');
    // }
    // }
  }
  videoEnded() {
    this.videoEnd = true;
  }
  getHeartbeat(){
    // alert("ss")
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '140');
    formData.append('audi', '3');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_16';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-60');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
  //  this.chatService.sendMessage(value, data.name, this.roomName);
  this._fd.postGroupchat(value,data.name,data.email, this.roomName, created,data.id).subscribe(res=>{
    console.log(res);
  })



    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}
