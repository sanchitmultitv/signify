import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeetingRoomComponent } from './meeting-room.component';


const routes: Routes = [
  {
    path:'', component: MeetingRoomComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeetingRoomRoutingModule { }
