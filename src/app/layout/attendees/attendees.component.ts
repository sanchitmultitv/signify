import { Component, OnInit, QueryList, ElementRef, ViewChildren, Renderer2 } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-attendees',
  templateUrl: './attendees.component.html',
  styleUrls: ['./attendees.component.scss']
})
export class AttendeesComponent implements OnInit {
  attendees = [];
  model: NgbDateStruct;
  date: { year: number, month: number };
  newTimeSlots: any = [];
  timeVal;
  uids;
  names: string;
  core_sector = new FormControl('');
  core_business = new FormControl('');
  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  constructor(private _fd: FetchDataService, private calendar: NgbCalendar, private renderer: Renderer2,private toastr: ToastrService) { }

  form = new FormGroup({
    core_sector: new FormControl(''),
    core_business: new FormControl('')
  });
  onSubmit() {
    if (this.form.invalid) {
      return;
    } else {
      this._fd.getReinvestAttendees(140, this.form.value.core_sector, this.form.value.core_business).subscribe((res: any) => {
        this.attendees = res.result;
        this.form.reset();
      });
    }

  }
  getUserid(id) {
    this.uids = id;
  }
  closeCall() {
    $('.scheduleCallmodal').modal('hide');
  }
  ngOnInit(): void {
    this.model = this.calendar.getToday();
    // console.log('todaydate', this.model);
    let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
   

    this.form.reset();
    // let event_id = 140;
    let event_id = 146;
    this._fd.getAttendees(event_id).subscribe((res: any) => {
      this.attendees = res.result;
    });
    this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
      console.log('timeresponse', res);
      this.newTimeSlots = res.result;
    });
  }
  // filterData(value) {
  //   let event_id = 140;
  //   if (value.length >= 3) {
  //     this._fd.getAttendeesbyName(event_id, value).subscribe((res: any) => {
  //       this.attendees = res.result;
  //       console.log(this.attendees);
  //     });
  //   }
  //   else {
  //     this._fd.getAttendees(event_id).subscribe((res: any) => {
  //       this.attendees = res.result;
  //     });
  //   }
  // }
  filterData(query) {
    // let event_id = 140;    
    let event_id = 146;    
      if(query!==''){
        if ((this.coreSectorvalue === null) && (this.coreBusinessvalue === null)) {
          this._fd.getAttendeesbyName(event_id, query).subscribe(res => {
            this.attendees = res.result;
          });
        }
        
        if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
          this._fd.ReinvestAttendees(event_id,this.coreSectorvalue, this.coreBusinessvalue,query).subscribe((res:any) => {
            this.attendees = res.result;
          });
        }
    }else {
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue === null)) {
        this._fd.getAttendees(event_id).subscribe(res => {
          this.attendees = res.result;
        });
      }
      
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
        this._fd.getReinvestAttendees(event_id,this.coreSectorvalue, this.coreBusinessvalue).subscribe((res:any) => {
          this.attendees = res.result;
        });
      }
    }
  }

  coreSectorvalue = null;
  coreBusinessvalue = null;
  
  clearAll() {
    this.coreSectorvalue = null;
    this.coreBusinessvalue = null;
    this.ngOnInit(); 
  }
  closePopup() {
    $('.attendeesModal').modal('hide');
  }
  getTime(event: any, valClass, time) {
    // console.log(time);
    this.timeVal = time;
    const hasClass = event.target.classList.contains(valClass);
    $(".time-list li a.active").removeClass("active");
    // adding classname 'active' to current click li
    this.renderer.addClass(event.target, valClass);
    // if (hasClass) {
    //   //alert('has')
    //       this.renderer.removeClass(event.target, valClass);
    //     } else {
    //      // alert(valClass)
    //       this.renderer.addClass(event.target, valClass);
    //     }
  }
  confirm() {
    // alert(this.dateModel);
    //console.log('val',dp);
    //this.route.paramMap.subscribe(params => {
    //this.exhibitionName = params.get('exhibitName');
    let data = JSON.parse(localStorage.getItem('virtual'));
    console.log(this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal);
    const CallData = new FormData();
    CallData.append('attendee_id', this.uids);
    CallData.append('user_id', data.id);
    CallData.append('time', this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal);
    this._fd.schdeuleAcallattende(CallData).subscribe(res => {
      console.log(res);
      if (res.code == 1) {
        // this.toastr.success('Login Successfully');

        let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
        //let myDate = '2020-10-25';
        let data = JSON.parse(localStorage.getItem('virtual'));
        this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
          console.log('timeresponse', res);
          this.newTimeSlots = res.result;
        })
        setTimeout(() => {
          $('.scheduleCallmodal').modal('hide');
        }, 2000);
      }

    });

    //});
  }
  onDateSelect(dates) {
    console.log('aycycyt', dates.year + '-' + dates.month + '-' + dates.day);
    let myDate = dates.year + '-' + dates.month + '-' + dates.day;
    //this.route.paramMap.subscribe(params => {
    //this.exhibitionName = params.get('exhibitName');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
      console.log('timeresponse', res);
      this.newTimeSlots = res.result;
    })
    //});


  }
}
