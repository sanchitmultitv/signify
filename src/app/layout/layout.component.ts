import { Component, OnInit, Renderer2, ViewChild, ElementRef, Output, HostListener, AfterViewInit } from '@angular/core';
import { fadeAnimation } from '../shared/animation/fade.animation';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';
import { AuthService } from '../services/auth.service';
import { FetchDataService } from '../services/fetch-data.service';
import { ToastrService } from 'ngx-toastr';
import { ChatService } from 'src/app/services/chat.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var $: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [fadeAnimation]

})
export class LayoutComponent implements OnInit, AfterViewInit {
  firstName;
  landscape = true;
  actives: any = [];
  activeAuditoriums = [];
  videoPlay2:any
   liveMsg = false;
  msg: any;
  gameSrc;
  getFrame;
  countdown=false;
  constructor(public router: Router, private renderer: Renderer2, private auth: AuthService, private _fd: FetchDataService,private toastr: ToastrService,private  chat: ChatService) { }

  ngOnInit(): void {
    // this.toastr.success( 'Login Successfully');
    this.audiActive();
    $('.videoData').show();
    // this.firstName = JSON.parse(localStorage.getItem('virtual')).name;
    this.firstName = JSON.parse(localStorage.getItem('virtual'));
    this.playAudio();

    
  
    
///////
    
    this.chat.getconnect('toujeo-146');
    this.chat.getMessages().subscribe((data => {
         console.log('data',data);
         let checkData = data.split('*');
         let mymail = localStorage.getItem('myemail');
         if(checkData[0] == mymail && checkData[1]!='exit'){
           localStorage.setItem('user_key', checkData[1]);
          var timeleft = 10;
          var downloadTimer = setInterval(() =>{
        if(timeleft <= 0){
          clearInterval(downloadTimer);
          document.getElementById("countdown").innerHTML = "";
          document.getElementById("countdown").style.display = "none";
          this.routing();
        } else {
          document.getElementById("countdown").style.display = "block";
          document.getElementById("countdown").innerHTML ="You are going live in " +timeleft + " seconds.";
          
        }
        console.log(timeleft);
        timeleft -= 1;
      }, 1000);
         }
         
        //10 seconds timer 
       
         
          this.msg = data;
         
         let dta = this.msg.split("start_live_");
         this.msg = dta[1];
        //  console.log('msg',this.msg,dta);
        
       if (data == 'start_live') {
       this.liveMsg = true;
       }
       document.getElementById("this.liveMsg");
      

       if (data == 'stop_live') {
         this.liveMsg = false;
       
       }
       
      }));
      function myFunction() {
        setInterval(function(){ ("'msg',this.msg,dta"); }, 1000);
      }
////////


    
  }
  routing(){
    //alert('hel');
    this.router.navigate(['/mainCall']);
  }
  ngAfterViewInit() {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
  }
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  showModal = false;
  toggleModal() {
    this.showModal = !this.showModal;
  }
  closemodal() {
    // location.reload();
    $('.lolModal').modal('hide');

  }
  readOutputValueEmitted(event) {
    console.log('kkkk', event)
  }

  videoWidth = 0;
  videoHeight = 0;
  showImage = false;
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  @Output('myOutputVal') myOutputVal = new EventEmitter();

  playAudio() {
    // let abc: any = document.getElementById('myAudio');
    // abc.play();
  }
  constraints = {
    video: {
      facingMode: "environment",
      width: { ideal: 720 },
      height: { ideal: 480 }
    }
  };
  audis = ['Audi 1', 'Audi 2'];
  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      console.log(res, 'resssss');
      this.actives = res.result;
      this.activeAuditoriums = res.result;
      console.log(this.actives[0]);
    })
  }
  gotoAudi(audi) {
    if (audi === 1) {
      localStorage.setItem('serdia_room', 'myanmar_16');
      if (this.actives[3].status == true) {
        this.router.navigate(['/auditorium/one']);
      }
      else {
        $('.audiModal').modal('show');
      }
    }
    if (audi === 2) {
      localStorage.setItem('serdia_room', 'myanmar_17');
      if (this.actives[2].status == true) {
        this.router.navigate(['/auditorium/two']);
      }
      else {
        $('.audiModal').modal('show');
      }
    }
    if (audi === 3) {
      localStorage.setItem('serdia_room', 'myanmar_18');
      if (this.actives[1].status == true) {
        this.router.navigate(['/auditorium/three']);
      }
      else {
        $('.audiModal').modal('show');

      }
    }
    if (audi === 4) {
      localStorage.setItem('serdia_room', 'myanmar_19');
      // this.router.navigate(['/auditorium/four']);
      if (this.actives[0].status == true) {
        this.router.navigate(['/auditorium/fourth-auditorium']);
      }
      else {
        $('.audiModal').modal('show');
      }

    }
  }

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
      alert('Sorry, camera not available.');
    }

  }
  handleError(error) {
    console.log('Error: ', error);
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    // formData.append('event_id', '140');
    formData.append('event_id', '146');
    
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }

  stream: any;
  attachVideo(stream) {
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
    this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }
  img;
  capture() {
    this.showImage = true;
    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
    let canvas: any = document.getElementById("canvas");
    let context = canvas.getContext('2d');
    context.strokeStyle = "#ffc90e";
    context.lineWidth = 5;
    var img = document.getElementById("logo");
    context.drawImage(img, 5, 5, 120, 50);
    context.strokeRect(0, 0, canvas.width, canvas.height);
    let imgPath = '../../../assets/img/xtrem.jpeg';
    let imgObj = new Image();

    imgObj.src = imgPath;
    this.img = canvas.toDataURL("image/png");
    console.log('kkkk', imgObj.src)
    // $('.videoData').hide();
  }
  selfie2(){
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // if (timer >= '16:45:00' ) {
    this.router.navigateByUrl('/capturePhoto2');
    // } else {
    // $('.note5').modal('show');
    // }
  }
  stopVideo() {
    this.renderer.listen(this.videoElement.nativeElement, 'stop', (event) => {
      console.log('dsfj')
    });
  }

  showCapture() {
    $('.capturePhoto').modal('show');
    this.startCamera();
  }
  closepopup(){
    $('.note5').modal('hide');

  }

  closeCamera() {
    location.reload();
    $('.capturePhoto').modal('hide');

  }
  openGame(){
    this.gameSrc= 'https://lagged.com/api/play2/superbike-hero2/';
    this.getFrame ='<iframe  width="100%" height="600"  src="' +this.gameSrc +'" frameborder="0" allowfullscreen=""></iframe>';
    $('#frames').html(this.getFrame);
    $('.gameModals').modal('show');
  }
  closegame(){
    
    $('.gameModals').modal('hide');
    $('#frames').html('');
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }

  openSidebar() {
    document.getElementById("mySidenav").style.width = "220px";
  }

  closeSidebar() {
    document.getElementById("mySidenav").style.width = "0";
  }
  skipButton() {
    // let pauseVideo: any = document.getElementById("myVideo");
    // pauseVideo.currentTime = 0;
    // pauseVideo.pause();
    this.videoPlay2=false;
    this.router.navigateByUrl('/lobby');
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else {
      this.landscape = true;
    }
  }
  logout() {
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.auth.logout(user_id).subscribe(res => {
      let signify_userid = JSON.parse(localStorage.getItem('signify_data')).id;
      const formData = new FormData();
      formData.append('user_id', signify_userid);
      formData.append('status', '0');
      this._fd.signify_logout(formData).subscribe((res:any)=>{
        console.log(res.result);
      });
      const formsData = new FormData();
      formsData.append('user_id', signify_userid);
      formsData.append('flag', '0');
      console.log(formsData);
      this._fd.signify_update(formsData).subscribe((res:any)=>{
        console.log(res.result);
      })
      this.router.navigate(['/login']);
      localStorage.clear();
    });
   
  }
}
