import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.component.html',
  styleUrls: ['./group-chat.component.scss']
})
export class GroupChatComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'signify';
  username:string=''
  flag: boolean = false;
  
  user_name;

  

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {

    // this.loadData();
   this.username = JSON.parse(localStorage.getItem('username'));
console.log(this.username);
  }

  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-146');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.user_name = data.name;
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
    //    this.chatGroup();
    // this.chatService.getconnect('toujeo-146');
    // this.chatService.getMessages().subscribe((data=>{

    //   console.log('socketdata', data);

    //   if(data == 'group_chat'){
    //     this.chatGroup();
    //   }

    // }));


    // let data = JSON.parse(localStorage.getItem('virtual'));
    // this.chatService.addUser(data.name, 'myanmar_16');
    // localStorage.setItem('username', data.name);
    // this.chatService
    //   .receiveMessages('myanmar_16')
    //   .subscribe((msgs: string) => {
    // this.messageList.push(msgs);
    //     console.log('demo',this.messageList);
    //   });
   



  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);

      this.messageList = res.result;
      console.log(this.messageList);
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');


    });
  }

  closePopup() {
    $('.groupchatsModal').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
  //  this.chatService.sendMessage(value, data.name, this.roomName);
  this._fd.postGroupchat(value, data.name, data.email, this.roomName, created,data.id).subscribe(res=>{
    console.log(res);
  })



    this.textMessage.reset();

    //  console.log(value);

    // let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(data);

    // this.chatService.sendMessage(value, data.name, 'myanmar_16');
    // console.log( this.roomName);

    // this.textMessage.reset();
    // this.chatGroup();
    // this.newMessage.push(this.msgs);



  }

}
