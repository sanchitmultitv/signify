import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-group-chat-two',
  templateUrl: './group-chat-two.component.html',
  styleUrls: ['./group-chat-two.component.scss']
})
export class GroupChatTwoComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any=[];
  roomName= 'myanmar_17';
  constructor(private chatService : ChatService, private _fd : FetchDataService) { }

  ngOnInit():void{
    // this.chatGroupTwo();
  }
  loadData(): void {
    
  //   this.chatGroupTwo();
  //   this.chatService.getconnect('toujeo-52');
  //   this.chatService.getMessages().subscribe((data=>{
      
  //     console.log('socketdata', data);
     
  //     if(data == 'group_chat'){
  //       this.chatGroupTwo();
  //     }
       
  //   }));
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //   this.chatService.addUser(data.name, 'myanmar_17');
  //   localStorage.setItem('username', data.name);
  //   this.chatService
  //     .receiveMessages('myanmar_17')
  //     .subscribe((msgs: string) => {
  //   this.messageList.push(msgs);
  //       console.log('demo',this.messageList);
  //     });
  }
  chatGroupTwo(){
    this._fd.groupchatingtwo().subscribe(res=>{
      console.log('res',res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');
      
    });
  }
  closePopup(){
    $('.groupchatsModalTwo').modal('hide');
  }
  postMessageTwo(value){
   // console.log(value);
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //  // console.log(data.name);
  //     this.chatService.sendMessage(value, data.name, 'myanmar_17');
  //     // console.log(this.roomName);
  //     this.textMessage.reset();
  //     this.chatGroupTwo();
      //this.newMessage.push(this.msgs);
  }
}
