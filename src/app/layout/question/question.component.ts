import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit, OnDestroy {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  exhibiton:any=[];
  documents:any=[];
  
  exhibition_id = '0';
  constructor(private _fd: FetchDataService, private chat: ChatService) { }

  ngOnInit(): void {
   this.getQA();
  //  this.chat.getconnect('toujeo-140');
  this.chat.getconnect('toujeo-146');
   this.chat.getMessages().subscribe((data=>{
      console.log('socketdata', data);
      let check = data.split('_');
      console.log(check+"yyyooyoy")
      if(check[0] == 'question'){
        this.getQA();
      }else{
        //this.getQA();
      }
    }));
    //  this.interval = setInterv al(() => {
    //   this.getQA();
    // }, 5000);

  }
  closePopup() {
    $('.liveQuestionModal').modal('hide');
  }
  // getExhibit(){
  //   this._fd.getExhibition('Adapt').subscribe(res=>{
      
  //     this.exhibiton = res.result[0];
  //     this.documents = res.result[0].document;
  //     this.exhibition_id = res.result[0].id;
  //     console.log('exhibition',this.exhibiton); 
  //     //localStorage.setItem('exhibitData',JSON.stringify(res.result));
  //   });
  // }

  // getQA(){
  //   setTimeout(() => {
  //     $('#chat_messagedd')[0].scrollTop = $('#chat_messagedd')[0].scrollHeight;
  //   }, 2000);
  // //  console.log('exhibitonid',this.exhibition_id);
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //  // console.log('uid',data.id);
  //   this._fd.getanswers(data.id,this.exhibition_id).subscribe((res=>{
  //     //console.log(res);
  //     this.qaList = res.result;
  //     // alert('hello');
  //   }))

  // }
  getQA(){
    //  console.log('exhibitonid',this.exhibition_id);
      let data = JSON.parse(localStorage.getItem('virtual'));
     // console.log('uid',data.id);
      this._fd.gethelpdeskanswers(data.id).subscribe((res=>{
        //console.log(res);
        this.qaList = res.result;
        // alert('hello');
      }))
  
    }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
  //  console.log(value, data.id);
  // this.getQA();
    this._fd.helpdesk(data.id,value).subscribe((res=>{
      //console.log(res);
      // this.getQA();
      if(res.code == 1){
        this.textMessage.reset();
        let arr ={
          "question": value,
          "answer": ""
        };
        this.qaList.push(arr);
        //this.toastr.success( '!');
      // var d = $('.chat_message');
      // d.scrollTop(d.prop("scrollHeight"))
      }
      //this.getQA();
       
    //  setTimeout(() => {
     //   $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
      //  this.msg = '';
//$('.liveQuestionModal').modal('hide');
  //    }, 2000);
      // setTimeout(() => {
      //   this.msg = '';
      //   $('.liveQuestionModal').modal('hide');
      // }, 2000);
     // this.textMessage.reset();
    }))
    

  }
//   postQuestion(value){
//     let data = JSON.parse(localStorage.getItem('virtual'));
//   //  console.log(value, data.id);
//   // this.getQA();
//     this._fd.askQuestions(data.id,value,this.exhibition_id).subscribe((res=>{
//       //console.log(res);
//       // this.getQA();
//       if(res.code == 1){
//         this.msg = 'Submitted Succesfully';
//       // var d = $('.chat_message');
//       // d.scrollTop(d.prop("scrollHeight"))
//       }
//       this.getQA();
       
//       setTimeout(() => {
//         this.msg = '';
//         $('#chat_messagedd')[0].scrollTop = $('#chat_messagedd')[0].scrollHeight;
//       }, 2000);
// //$('.liveQuestionModal').modal('hide');
      
//       // setTimeout(() => {
//       //   this.msg = '';
//       //   $('.liveQuestionModal').modal('hide');
//       // }, 2000);
//       this.textMessage.reset();
//     }))
    

//   }
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
