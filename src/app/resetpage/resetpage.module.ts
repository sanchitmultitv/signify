import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ResetpageRoutingModule } from './resetpage-routing.module';
import { ResetpageComponent } from './resetpage.component';


@NgModule({
  declarations: [ResetpageComponent],
  imports: [
    CommonModule,
    ResetpageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ResetpageModule { }
