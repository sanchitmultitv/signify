import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService as AuthGuard } from '../app/services/guard.service';
import { BriefcaseComponent } from './core-components/briefcase/briefcase.component';
// import {AuthGuard} from './shared/auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'prefix' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
  { path: '', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule),},
  { path: 'resetpage', loadChildren: () => import('./resetpage/resetpage.module').then(m => m.ResetpageModule) },
  { path: 'briefcase', component:BriefcaseComponent},
  { path: 'hallway', loadChildren: () => import('./core-components/hallway/hallway.module').then(m => m.HallwayModule) }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {'useHash': true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
